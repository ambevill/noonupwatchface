using Toybox.ActivityMonitor as ActivityMonitor;


class MoveBarWidget {

    static const mb_left_x = 75;
    static const mb_bottom_y = 200;
    static const mb_width = 8;
    static const mb_cell_height = 7;
    static const mb_point_height = 3;
    static const mb_spacing = 2;


    function initialize() {}

    function draw(dc) {

        /* Move bar */

        // https://developer.garmin.com/connect-iq/api-docs/Toybox/ActivityMonitor/Info.html
        // https://www8.garmin.com/manuals/webhelp/vivofit4/EN-US/GUID-3919E821-56C5-474F-A014-126FED56C15A.html
        // watch settings -> Activity Tracking -> Move Alert (enable)

        var mb_level = ActivityMonitor.getInfo().moveBarLevel;

        if (mb_level == ActivityMonitor.MOVE_BAR_LEVEL_MIN) {
            return;
        }

        dc.setAntiAlias(true);

        dc.setColor(0xffff00, 0xffff00);
        var very_top = mb_bottom_y - mb_cell_height + 3 * ( - mb_cell_height + mb_point_height - mb_spacing);
        dc.fillPolygon([
            [mb_left_x, mb_bottom_y],
            [mb_left_x, very_top + mb_point_height],
            [mb_left_x + mb_width / 2, very_top],
            [mb_left_x + mb_width, very_top + mb_point_height],
            [mb_left_x + mb_width, mb_bottom_y]]);

        var mb_bottom_y_shifted = mb_bottom_y;

        dc.setColor(0xff0000, 0xff0000);
        for (var i_mb = 0; i_mb < mb_level - 1; ++i_mb) {

            dc.fillPolygon([
                [mb_left_x, mb_bottom_y_shifted],
                [mb_left_x, mb_bottom_y_shifted - mb_cell_height + mb_point_height],
                [mb_left_x + mb_width / 2, mb_bottom_y_shifted - mb_cell_height],
                [mb_left_x + mb_width, mb_bottom_y_shifted - mb_cell_height + mb_point_height],
                [mb_left_x + mb_width, mb_bottom_y_shifted],
                [mb_left_x + mb_width / 2, mb_bottom_y_shifted - mb_point_height]]);

            mb_bottom_y_shifted = mb_bottom_y_shifted - mb_cell_height + mb_point_height - mb_spacing;
        }

        dc.setAntiAlias(false);
    }
}