using Toybox.Math as Math;
// using Toybox.Graphics as Graphics;
using Toybox.Application as App;
// using Toybox.SensorHistory as SensorHistory;
using Toybox.System as System;
using Toybox.Time as Time;
using Toybox.Lang as Lang;


class MinuteUI {

    hidden var current_minute;
    hidden var pointer_width_degrees, pointer_radial_height_px;

    // constants, to be calculated only in the first draw()
    // screen geometry
    hidden var center_x, center_y, radius;

    // graphics parameters, to be recalculated every minute; it doesn't
    // make much sense to buffer this, since it will not be redrawn
    // every second in low-power mode
    hidden var precalc_hour, precalc_minute;
    hidden var pointer_polygon;

    hidden var battery_rate_widget, move_bar_widget, recovery_time_widget;


    function initialize(dc) {
        current_minute = 0.0;
        pointer_width_degrees = 5.0;
        pointer_radial_height_px = 8;

        center_x = dc.getWidth() / 2.0 + 0.5;
        center_y = dc.getHeight() / 2.0 + 0.5;
        radius = dc.getWidth() / 2.0;

        battery_rate_widget = new BatteryRateWidget(120, 182);
        move_bar_widget = new MoveBarWidget();
        recovery_time_widget = new RecoveryTimeWidget(120, 182 - 19);
    }

    function precalc_minutely(dc, clockTime) {
        // System.println("HP minutely");
        precalc_hour = clockTime.hour;
        precalc_minute = clockTime.min;

        var radial_thickness_px = App.Properties.getValue("arc_radial_thickness_px");

        pointer_polygon = triangle_coords(
            radius - radial_thickness_px,
            radius - radial_thickness_px - pointer_radial_height_px,
            precalc_hour + precalc_minute / 60.0,
            pointer_width_degrees
        );
    }

    function draw(dc, clockTime) {
        // if (center_x == null) { precalc_constants(dc); }
        if (precalc_minute != clockTime.min || precalc_hour != clockTime.hour) { precalc_minutely(dc, clockTime); }

        // System.println("Drawing minute UI de novo.");

        dc.setAntiAlias(true);

        // pointer

        var pointer_color = App.Properties.getValue("hour_pointer_color");
        dc.setColor(pointer_color, pointer_color);
        dc.fillPolygon(pointer_polygon);

        // center text: time

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        dc.drawText(
            center_x + 72, center_y,
            Graphics.FONT_SYSTEM_NUMBER_HOT,
            Lang.format("$1$:$2$", [clockTime.hour.format("%2d"), clockTime.min.format("%02d")]),
            Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER
        );


        // to do: other metrics (in various widget classes)

        /*var body_battery = SensorHistory
            .getBodyBatteryHistory({:period => 1, :order => SensorHistory.ORDER_NEWEST_FIRST})
            .next()
            .data;
        // System.println("BodyBattery: " + body_battery);  // print the current sample

        var stress = SensorHistory
            .getStressHistory({:period => 1, :order => SensorHistory.ORDER_NEWEST_FIRST})
            .next()
            .data;
        // System.println("Stress: " + stress);  // print the current sample

        // dc.drawBitmap(center_x, center_y, body_battery_logo);
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            center_x, 172,
            Graphics.FONT_SYSTEM_SMALL,
            Lang.format("BB $1$ | S $2$", [
                body_battery.format("%02d"),
                stress.format("%02d")
            ]),
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );*/


        /* other widgets */

        battery_rate_widget.draw(
            dc,
            System.getSystemStats().battery as Lang.Float,
            Time.now());

        move_bar_widget.draw(dc);

        recovery_time_widget.draw(dc);

        dc.setAntiAlias(false);
    }

    function triangle_coords(rad1, rad2, hour, width_degrees) {

        var deg = hour_to_degree(hour);
        return [
            [center_x + rad1 * Math.cos(deg * deg_to_rad), center_y - rad1 * Math.sin(deg * deg_to_rad)],
            [center_x + rad2 * Math.cos((deg + width_degrees / 2.0) * deg_to_rad), center_y - rad2 * Math.sin((deg + width_degrees / 2.0) * deg_to_rad)],
            [center_x + rad2 * Math.cos((deg - width_degrees / 2.0) * deg_to_rad), center_y - rad2 * Math.sin((deg - width_degrees / 2.0) * deg_to_rad)]];

    }
}