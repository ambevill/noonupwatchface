import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;

class NoonupWatchfaceApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    //! Return the initial view for the app
    //! @return Array Pair [View, Delegate] or Array [View]
    public function getInitialView() as Array<Views or InputDelegates>? {
        if (WatchUi has :WatchFaceDelegate) {
            var view = new $.NoonupWatchfaceView();
            var delegate = new $.NoonupWatchfaceDelegate(view);
            return [view, delegate] as Array<Views or InputDelegates>;
        } else {
            return [new $.NoonupWatchfaceView()] as Array<Views>;
        }
    }
}

function getApp() as NoonupWatchfaceApp {
    return Application.getApp() as NoonupWatchfaceApp;
}