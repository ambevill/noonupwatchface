using Toybox.System;
using Toybox.Time;
using Toybox.Time.Gregorian;
using Toybox.Test;

function is_leap_year(year) {
    if (year % 4 != 0) {return false;}
    if (year % 100 != 0) {return true;}
	if (year % 400 != 0) {return false;}
	return true;
}

const DOY_START_DAY = [
    0,
    31,
    31 + 28,
    31 + 28 + 31,
    31 + 28 + 31 + 30,
    31 + 28 + 31 + 30 + 31,
    31 + 28 + 31 + 30 + 31 + 30,
    31 + 28 + 31 + 30 + 31 + 30 + 31,
    31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
    31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
    31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
    31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
];

function day_of_year(year, month, day) {
    return day + DOY_START_DAY[month - Gregorian.MONTH_JANUARY] + ((is_leap_year(year) && (month > Gregorian.MONTH_FEBRUARY)) ? 1 : 0);
}

function week_of_year(year, month, day, garmin_dow) {
    var x_iso_dow = (garmin_dow - Gregorian.DAY_MONDAY + 7) % 7 + 1;  // Monday = 1, Sunday = 7
    // the nth Thursday always falls in the nth week of the year
    var nearest_thursday_day_of_year = day_of_year(year, month, day) - x_iso_dow + 4;
    if (nearest_thursday_day_of_year <= 0) {
        year -= 1;
        nearest_thursday_day_of_year += (is_leap_year(year) ? 366 : 365);
    } else if (nearest_thursday_day_of_year >= (is_leap_year(year) ? 366 : 365)) {
        return 1;
    }
    return (nearest_thursday_day_of_year + 6) / 7;
}

function test_week_of_year() {

    System.println("begin tests");
    
    // https://en.wikipedia.org/wiki/ISO_week_date

    Test.assertEqual(53, week_of_year(1977, 1, 2, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(1977, 1, 3, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(1978, 1, 1, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(1978, 1, 2, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(1978, 12, 31, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(1979, 1, 1, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(1979, 12, 30, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(1979, 12, 31, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(1980, 12, 28, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(1980, 12, 29, Gregorian.DAY_MONDAY));

    Test.assertEqual(53, week_of_year(1982, 1, 3, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(1982, 1, 4, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(2000, 1, 2, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(2000, 1, 3, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(2000, 12, 31, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(2001, 1, 1, Gregorian.DAY_MONDAY));

    Test.assertEqual(52, week_of_year(2023, 1, 1, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 1, week_of_year(2023, 1, 2, Gregorian.DAY_MONDAY));
    Test.assertEqual( 1, week_of_year(2023, 1, 8, Gregorian.DAY_SUNDAY));
    Test.assertEqual( 2, week_of_year(2023, 1, 9, Gregorian.DAY_MONDAY));

    System.println("end tests");
}