using Toybox.Math as Math;
using Toybox.Graphics as Gfx;
using Toybox.Activity as Act;
using Toybox.Time as Time;
using Toybox.Application as App;
using Toybox.Lang as Lang;


function hour_to_degree(hour) {
    return hour / 24.0 * -360 + 270;
}

const deg_to_rad = Math.PI / 180.0;

class DailyBufferedUI {

    hidden var sun_calc;
    // screen geometry
    hidden var center_x, center_y, arc_center_x, arc_center_y, radius;
    // ticks on the hours
    hidden var tick_x1_array as Lang.Array<Lang.Float>, tick_x2_array as Lang.Array<Lang.Float>, tick_y1_array as Lang.Array<Lang.Float>, tick_y2_array as Lang.Array<Lang.Float>;

    // graphics parameters, to be recalculated daily; ideally just the buffer
    // is needed, but the others are cached as a fallback
    hidden var precalc_daily_day;
    hidden var sunevent_degrees as Lang.Array<Lang.Float>;
    hidden var buffer;

    function initialize(dc) {
        sun_calc = new SunCalc();

        arc_center_x = dc.getWidth() / 2.0;
        arc_center_y = dc.getHeight() / 2.0;
        center_x = arc_center_x + 0.5;
        center_y = arc_center_y + 0.5;
        radius = dc.getWidth() / 2.0;

        sunevent_degrees = [0.0, 0.0, 0.0, 0.0];  // init required

        tick_x1_array = new[12];
        tick_x2_array = new[12];
        tick_y1_array = new[12];
        tick_y2_array = new[12];

        for (var i = 0; i < 12; ++i) {
            var theta = hour_to_degree(i) * deg_to_rad;
            var rcos_theta = radius * 1.1 * Math.cos(theta);
            var rsin_theta = radius * 1.1 * Math.sin(theta);

            tick_x1_array[i] = center_x + rcos_theta;
            tick_x2_array[i] = center_x - rcos_theta;
            tick_y1_array[i] = center_y - rsin_theta;
            tick_y2_array[i] = center_y + rsin_theta; 
        }

        // If this device supports BufferedBitmap, allocate the buffers we use for drawing
        if (Gfx has :BufferedBitmap) {
            // Allocate a buffer for the arcs, tics, and date. Use a full color
            // palette because these are anti-aliased.
            buffer = new Gfx.BufferedBitmap({
                :width=>dc.getWidth(),
                :height=>dc.getHeight()
            });
        } else {
            buffer = null;
        }
    }

    function precalc_daily(dc, today, today_short) {
        // System.println("SEA daily");
        precalc_daily_day = today.day;
        sunevent_degrees = [
            get_sunevent_degree(DAWN),
            get_sunevent_degree(SUNRISE),
            get_sunevent_degree(SUNSET),
            get_sunevent_degree(DUSK)];

        if (buffer != null) {
            redraw(buffer.getDc(), today, today_short);
        }
    }

    function draw(dc, today, today_short) {
        // if (center_x == null) { precalc_constants(dc); }
        if (precalc_daily_day != today.day) { precalc_daily(dc, today, today_short); }

        if (buffer == null) {
            // System.println("Drawing daily UI de novo.");
            redraw(dc, today, today_short);
        } else {
            // System.println("Drawing daily UI from buffer.");
            dc.drawBitmap(0, 0, buffer);
        }
    }

    function redraw(dc, today, today_short) {
        // System.println("Redrawing daily buffer/dc.");

        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();

        dc.setAntiAlias(true);

        var radial_thickness_px = App.Properties.getValue("arc_radial_thickness_px");

        // built-in colors: https://developer.garmin.com/connect-iq/api-docs/Toybox/Graphics.html

        // arcs

        dc.setPenWidth(radial_thickness_px);
        // night
        dc.setColor(0x55AAFF, 0x55AAFF);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius - radial_thickness_px * 0.5,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[3],
            sunevent_degrees[0]);
        // dawn
        dc.setColor(0xFF5500, 0xFF5500);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius - radial_thickness_px * 0.5,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[0],
            sunevent_degrees[1]);
        // dusk
        // dc.setColor(0xFF5500, 0xFF5500);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius - radial_thickness_px * 0.5,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[2],
            sunevent_degrees[3]);
        // day
        dc.setColor(0xFFAA00, 0xFFAA00);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius - radial_thickness_px * 0.5,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[1],
            sunevent_degrees[2]);

        // minor ticks over the arcs

        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_BLACK);
        dc.setPenWidth(2);
        for (var i = 0; i < 12; ++i) {
            if (i % 3 == 0) { continue; }
            dc.drawLine(
                tick_x1_array[i],
                tick_y1_array[i],
                tick_x2_array[i],
                tick_y2_array[i]);
        }

        // arcs again to half-cover the minor ticks
        dc.setPenWidth(radial_thickness_px);
        // night
        dc.setColor(0x55AAFF, 0x55AAFF);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[3],
            sunevent_degrees[0]);
        // dawn
        dc.setColor(0xFF5500, 0xFF5500);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[0],
            sunevent_degrees[1]);
        // dusk
        // dc.setColor(0xFF5500, 0xFF5500);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[2],
            sunevent_degrees[3]);
        // day
        dc.setColor(0xFFAA00, 0xFFAA00);
        dc.drawArc(
            arc_center_x,
            arc_center_y,
            radius,
            Graphics.ARC_CLOCKWISE,
            sunevent_degrees[1],
            sunevent_degrees[2]);

        // major ticks

        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_BLACK);
        dc.setPenWidth(2);
        for (var i = 0; i < 12; i += 3) {
            dc.drawLine(
                tick_x1_array[i],
                tick_y1_array[i],
                tick_x2_array[i],
                tick_y2_array[i]);
        }

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        // top text: year, week number
        dc.drawText(
            center_x, 39,
            Graphics.FONT_SYSTEM_XTINY,
            Lang.format("$1$-W$2$", [
                today.year.format("%d"),
                week_of_year(today_short.year, today_short.month, today_short.day, today_short.day_of_week).format("%02d")]),
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );

        // row-2 text: date, day of week
        dc.drawText(
            center_x, 72,
            Graphics.FONT_SYSTEM_LARGE,
            Lang.format("$3$ $1$ $2$", [
                today.month,
                today.day,
                today.day_of_week]),
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );

        dc.setAntiAlias(false);
    }

    // adapted from https://github.com/OliverHannover/Aviatorlike/blob/master/source/marker.mc
    function get_sunevent_degree(what) {

        var lat;
		var lon;
        var loc = Act.getActivityInfo().currentLocation;

		if (loc == null)
		{
			lat = App.Properties.getValue("default_lat") * deg_to_rad;
			lon = App.Properties.getValue("default_lon") * deg_to_rad;
		}
		else
		{
			App.Properties.setValue("default_lat", loc.toDegrees()[0]);
			App.Properties.setValue("default_lon", loc.toDegrees()[1]);
			lat = App.Properties.getValue("default_lat") * deg_to_rad;
			lon = App.Properties.getValue("default_lon") * deg_to_rad;
		}

		if(lat != null && lon != null)
		{
			var now = new Time.Moment(Time.now().value());
            var moment = sun_calc.calculate(now, lat.toDouble(), lon.toDouble(), what);
			var Tinfo = Time.Gregorian.info(new Time.Moment(moment.value() + 30), Time.FORMAT_SHORT);
    		return hour_to_degree(Tinfo.hour + Tinfo.min / 60.0);
        } else {
            return 0.0;
        }
    }
}