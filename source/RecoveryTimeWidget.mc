using Toybox.Lang as Lang;
using Toybox.ActivityMonitor as ActivityMonitor;
using Toybox.Graphics as Graphics;
using Toybox.WatchUi as Ui;


class RecoveryTimeWidget {

    hidden var anchor_x, anchor_y;

    hidden var rtr_white, rtr_green, rtr_orange, rtr_red;

    function initialize(anchor_x_, anchor_y_) {
        anchor_x = anchor_x_;
        anchor_y = anchor_y_;
    }

    function draw(dc) {

        var xmin = anchor_x - 21,
            xmax = xmin + 10,
            ymin = anchor_y + 4,
            ymax = ymin + 14;

        var ttr = ActivityMonitor.getInfo().timeToRecovery as Lang.Number;
        // ttr = 50;
        if (ttr != null) {
            /*
            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
            dc.drawText(
                anchor_x, anchor_y,
                Graphics.FONT_SYSTEM_XTINY,
                Lang.format("$1$h", [
                    ttr.format("%d")]),
                Graphics.TEXT_JUSTIFY_LEFT
            );
            */

            if (ttr == 0) {
                /*
                if (rtr_white == null) {
                    rtr_white = Ui.loadResource(Rez.Drawables.rtr_white);
                }
                dc.drawBitmap(anchor_x-28-4, anchor_y-2, rtr_white);
                */
            } else if (ttr <= 24) {
                // green, e.g. https://support.garmin.com/en-US/?faq=agZJiZRjhX2adgWkBVOHI9
                if (rtr_green == null) {
                    rtr_green = Ui.loadResource(Rez.Drawables.rtr_green);
                }
                dc.drawBitmap(anchor_x-28-4, anchor_y-2, rtr_green);
            } else if (ttr <= 48) {
                // orange, e.g. https://www.garmin.com/en-US/garmin-technology/running-science/physiological-measurements/recovery-time/
                if (rtr_orange == null) {
                    rtr_orange = Ui.loadResource(Rez.Drawables.rtr_orange);
                }
                dc.drawBitmap(anchor_x-28-4, anchor_y-2, rtr_orange);
            } else {
                if (rtr_red == null) {
                    rtr_red = Ui.loadResource(Rez.Drawables.rtr_red);
                }
                dc.drawBitmap(anchor_x-28-4, anchor_y-2, rtr_red);
            }
        }
    }
}