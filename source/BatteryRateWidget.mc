using Toybox.Application.Storage as Storage;
using Toybox.Time as Time;
using Toybox.Lang as Lang;
using Toybox.ActivityMonitor as ActivityMonitor;


class BatteryRateWidget {

    hidden var latest_battery_moment, latest_battery_pct;
    hidden var battery_anchor_x, battery_anchor_y;

    function initialize(battery_anchor_x_, battery_anchor_y_) {

        if (Storage.getValue("discharged_battery_pct") == null) {
            Storage.setValue("discharged_battery_pct", 0.0);
        }
        if (Storage.getValue("discharged_battery_hours") == null) {
            Storage.setValue("discharged_battery_hours", 0.0);
        }

        latest_battery_moment = Time.now() as Time.Moment;
        latest_battery_pct = System.getSystemStats().battery as Lang.Float;
        battery_anchor_x = battery_anchor_x_;
        battery_anchor_y = battery_anchor_y_;

    }

    function draw(dc, battery_pct, battery_moment) {

        var discharged_battery_pct = Storage.getValue("discharged_battery_pct");
        var discharged_battery_hours = Storage.getValue("discharged_battery_hours");

        // track discharge statistics iff not charging
        if (System.getSystemStats().charging as Lang.Boolean) {

            // discard old status so the entire time period is not logged
            latest_battery_pct = null;
            latest_battery_moment = null;

        } else {

            if (latest_battery_pct != null) {

                // half-life = 9 days (roughly 1 battery cycle)
                // 0.5 ^ (1 / (9*24*60))
                discharged_battery_pct = 0.99995 * discharged_battery_pct + latest_battery_pct - battery_pct;
                discharged_battery_hours = 0.99995 * discharged_battery_hours + battery_moment.subtract(latest_battery_moment).divide(3600.0).value();

                Storage.setValue("discharged_battery_pct", discharged_battery_pct);
                Storage.setValue("discharged_battery_hours", discharged_battery_hours);

            }

            latest_battery_pct = battery_pct;
            latest_battery_moment = battery_moment;

        }

        var capacity_hours = discharged_battery_pct == 0 ? 999 : discharged_battery_hours / discharged_battery_pct;
        // capacity_hours = 8*0.24;  // testing!!!

        // convert 100% to 1 charge
        var remaining_hours = capacity_hours * battery_pct;
        capacity_hours = capacity_hours * 100;

        var bordercolor,
            text;
        if (remaining_hours < 72) {
            bordercolor = remaining_hours < 36 ? 0xff0000 : 0xffff00;
            text = Lang.format("$1$ h", [remaining_hours.format("%.0f")]);
        } else {
            bordercolor = 0x005500;
            text = Lang.format("$1$ d/c", [
                    // convert 24 hours to 1 day
                    (capacity_hours / 24).format("%.1f")]);
        }

        dc.setAntiAlias(false);

        var xmax = battery_anchor_x - 6,
            xmin = xmax - 26,
            ymin = battery_anchor_y + 6,
            ymax = ymin + 10;

        dc.setPenWidth(1);

        dc.setColor(bordercolor, Graphics.COLOR_TRANSPARENT);
        dc.drawRectangle(
            xmin, ymin,
            xmax-xmin, ymax-ymin);
        // cathode
        dc.drawLine(
            xmax + 1, ymin + 2,
            xmax + 1, ymax - 2);
        if (remaining_hours < 36) {
            dc.setColor(0x550000, 0x000000);
            dc.fillRectangle(
                xmin + 2,
                ymin + 2,
                xmax-xmin-4,
                ymax - ymin - 4);
            dc.setColor(bordercolor, Graphics.COLOR_TRANSPARENT);
        }
        dc.fillRectangle(
            xmin + 2,
            ymin + 2,
            ((xmax-xmin-4) * battery_pct / 100 + 0.5).toNumber(),
            ymax - ymin - 4);

        dc.setAntiAlias(true);

        // dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            battery_anchor_x, battery_anchor_y,
            Graphics.FONT_SYSTEM_XTINY,
            text,
            Graphics.TEXT_JUSTIFY_LEFT
        );
    }
}