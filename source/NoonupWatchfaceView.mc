import Toybox.Graphics;
import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;
using Toybox.Position;
using Toybox.Time.Gregorian as Gregorian;
using Toybox.SensorHistory;

class NoonupWatchfaceView extends WatchUi.WatchFace {

    hidden var daily_buffered_ui, minute_ui, custom_metrics;
    hidden var center_x, center_y;
    // parameters for the onPartialUpdate clip box
    hidden var opu_x, opu_y, opu_width, opu_height;
    hidden var last_full_update_minute;

    function initialize() {
        WatchFace.initialize();
        // test_week_of_year();

        last_full_update_minute = null;
    }

    // Load your resources here
    function onLayout(dc as Dc) as Void {
        center_x = dc.getWidth() / 2.0 + 0.5;
        center_y = dc.getHeight() / 2.0 + 0.5;

        // onLayout is called roughly as often as initialize(), so it's ok
        // to just init and do the one-time precalcs in one place
        daily_buffered_ui = new DailyBufferedUI(dc);
        minute_ui = new MinuteUI(dc);

        opu_x = 193;
        opu_y = center_y - 18;
        opu_width = 23;
        opu_height = 20;
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        // System.println("onUpdate");

        var clockTime = System.getClockTime();

        // if (true) {
        if (last_full_update_minute != clockTime.min) {
            last_full_update_minute = clockTime.min;

            dc.clearClip();

            var now = Time.now();
            var today = Gregorian.info(now, Time.FORMAT_MEDIUM);
            var today_short = Gregorian.info(now, Time.FORMAT_SHORT);

            daily_buffered_ui.draw(dc, today, today_short);
            minute_ui.draw(dc, clockTime);

            dc.setClip(opu_x, opu_y, opu_width, opu_height);
            dc.setAntiAlias(false);
        }

        onPartialUpdate(dc);
    }

    public function onPartialUpdate(dc as Dc) as Void {

        // System.println("oPU");

        // dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        // dc.fillRectangle(200, center_y - 9, 223 - 200, 19);

        // dc.setColor(0xFF5500, Graphics.COLOR_BLACK);
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        dc.drawText(
            opu_x + 1, opu_y + opu_height / 2,
            Graphics.FONT_SYSTEM_TINY,
            System.getClockTime().sec.format("%02d"),
            Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER
        );

    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() as Void {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() as Void {
    }

}


//! Receives watch face events
class NoonupWatchfaceDelegate extends WatchUi.WatchFaceDelegate {
    private var _view as NoonupWatchfaceView;

    //! Constructor
    //! @param view The analog view
    public function initialize(view as NoonupWatchfaceView) {
        WatchFaceDelegate.initialize();
        _view = view;
    }

    //! The onPowerBudgetExceeded callback is called by the system if the
    //! onPartialUpdate method exceeds the allowed power budget. If this occurs,
    //! the system will stop invoking onPartialUpdate each second, so we notify the
    //! view here to let the rendering methods know they should not be rendering a
    //! second hand.
    //! @param powerInfo Information about the power budget
    public function onPowerBudgetExceeded(powerInfo as WatchFacePowerInfo) as Void {
        System.println("Average execution time: " + powerInfo.executionTimeAverage);
        System.println("Allowed execution time: " + powerInfo.executionTimeLimit);
        // _view.turnPartialUpdatesOff();
    }
}
